use std::io::BufRead;

// Usage example:
//
// let stdin = io::stdin();
// buf_read::basic(&mut stdin.lock());

pub fn basic(reader: impl BufRead) {
    for line in reader.lines() {
        println!("Oh, a line! '{}'", line.unwrap());
    }
}
