pub fn both() {
    let mut first = [1, 2, 3].iter();
    let mut second = [11, 22, 33].iter();

    let mut a = first.next();
    let mut b = second.next();

    let mut x = 0;

    while a != None && b != None {
        println!("{:?} {:?}", a, b);

        a = first.next();
    }
}
