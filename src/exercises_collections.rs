use std::io::{self, Write};
use std::collections::HashMap;

fn mean(slice: &[i32]) -> f32 {
    let mut sum = 0;
    for element in slice {
        sum += element;
    }
    return sum as f32 / slice.len() as f32;
}

#[cfg(test)]
mod test_mean {
    use super::mean;

    #[test]
    fn one() {
        assert_eq!(1.0, mean(&vec![1]));
    }

    #[test]
    fn two() {
        assert_eq!(1.5, mean(&vec![1, 2]));
        assert_eq!(15.0, mean(&vec![20, 10]));
        assert_eq!(100.0, mean(&vec![100, 100]));
    }

    #[test]
    fn three() {
        assert_eq!(2.0, mean(&vec![1, 2, 3]));
        assert_eq!(20.0, mean(&vec![10, 30, 20]));
        assert_eq!(200.0, mean(&vec![200, 100, 300]));
        assert_eq!(2_000.0, mean(&vec![2_000, 3_000, 1_000]));
        assert_eq!(20_000.0, mean(&vec![30_000, 10_000, 20_000]));
        assert_eq!(200_000.0, mean(&vec![300_000, 200_000, 100_000]));
    }
}

fn merge_sort(slice: &[i32]) -> Vec<i32> {
    if slice.len() < 2 {
        return slice.to_vec();
    }
    let mid = slice.len() / 2 as usize;
    let a = merge_sort(&slice[..mid]);
    let b = merge_sort(&slice[mid..]);
    let mut a_index = 0;
    let mut b_index = 0;
    let mut merged = Vec::new();

    while a_index + b_index < slice.len() {
        match (a.get(a_index), b.get(b_index)) {
            (Some(&a_elem), Some(&b_elem)) => {
                if a_elem < b_elem {
                    merged.push(a_elem);
                    a_index += 1;
                } else {
                    merged.push(b_elem);
                    b_index += 1;
                }
            },
            (None, Some(&b_elem)) => {
                merged.push(b_elem);
                b_index += 1;
            },
            (Some(&a_elem), None) => {
                merged.push(a_elem);
                a_index += 1;
            },
            (None, None) => {}
        }
    }

    merged
}

fn median(slice: &[i32]) -> f32 {
    let sorted = merge_sort(slice);
    let mid = slice.len() / 2 as usize;
    if sorted.len() % 2 == 1 {
        sorted[mid] as f32
    } else {
        mean(&sorted[mid-1..mid+1])
    }
}

#[cfg(test)]
mod tests_median {
    use super::median;

    #[test]
    fn one() {
        assert_eq!(1.0, median(&vec![1]));
    }

    #[test]
    fn two() {
        assert_eq!(1.5, median(&vec![1, 2]));
        assert_eq!(15.0, median(&vec![20, 10]));
        assert_eq!(100.0, median(&vec![100, 100]));
    }

    #[test]
    fn three() {
        assert_eq!(2.0, median(&vec![1, 2, 3]));
        assert_eq!(20.0, median(&vec![10, 30, 20]));
        assert_eq!(200.0, median(&vec![200, 100, 300]));
        assert_eq!(2_000.0, median(&vec![2_000, 3_000, 1_000]));
        assert_eq!(20_000.0, median(&vec![30_000, 10_000, 20_000]));
        assert_eq!(200_000.0, median(&vec![300_000, 200_000, 100_000]));
    }
}

fn mode(slice: &[i32]) -> i32 {
    let mut counts = HashMap::new();
    let mut max_count = 0;
    let mut candidate = 0;

    for value in slice {
        let count = counts.entry(value).or_insert(0);
        *count += 1;
        if *count > max_count {
            max_count = *count;
            candidate = *value;
        }
    }

    candidate
}

#[cfg(test)]
mod tests_mode {
    use super::mode;

    #[test]
    fn one() {
        assert_eq!(1, mode(&vec![1]));
    }

    #[test]
    fn two() {
        assert_eq!(1, mode(&vec![1, 2]));
        assert_eq!(20, mode(&vec![20, 10]));
        assert_eq!(100, mode(&vec![100, 100]));
    }

    #[test]
    fn three() {
        assert_eq!(1, mode(&vec![1, 2, 3]));
        assert_eq!(10, mode(&vec![10, 30, 20]));
        assert_eq!(200, mode(&vec![200, 100, 300]));
        assert_eq!(2_000, mode(&vec![2_000, 3_000, 1_000]));
        assert_eq!(30_000, mode(&vec![30_000, 10_000, 20_000]));
        assert_eq!(300_000, mode(&vec![300_000, 200_000, 100_000]));

        assert_eq!(3, mode(&vec![2, 3, 3]));
        assert_eq!(30, mode(&vec![30, 20, 30]));
        assert_eq!(300, mode(&vec![300, 300, 200]));
    }
}

pub fn solve_one(numbers: Vec<i32>) {
    println!("{:?}", numbers);
    println!("  Mean: {}", mean(&numbers));
    println!("  Median: {}", median(&numbers));
    println!("  Mode: {}", mode(&numbers));
}

const VOWELS: [char; 6] = ['a', 'e', 'i', 'o', 'u', 'y'];

fn is_vowel(character: char) -> bool {
    let lowered = character.to_ascii_lowercase();
    for vowel in VOWELS.iter() {
        if lowered == *vowel {
            return true;
        }
    }

    false
}

pub fn solve_two(text: &str) -> String {
    let mut solution = String::new();
    let mut maybe_previous: Option<char> = None;
    let mut maybe_word_start = None;

    for char in format!("{} ", text).chars() {
        let maybe_more = match (maybe_previous, maybe_word_start) {
            (None, _) => {
                if is_vowel(char) {
                    maybe_word_start = Some('h');
                    Some(String::from(char))
                } else if !char.is_ascii_whitespace() {
                    maybe_word_start = Some(char);
                    None
                } else {
                    Some(String::from(char))
                }
            },
            (Some(_previous), None) => {
                if is_vowel(char) {
                    maybe_word_start = Some('h');
                    Some(String::from(char))
                } else if !char.is_ascii_whitespace() {
                    maybe_word_start = Some(char);
                    None
                } else {
                    Some(String::from(char))
                }

            },
            (Some(previous), Some(word_start)) => {
                if char.is_ascii_whitespace() && !previous.is_ascii_whitespace() {
                    maybe_word_start = None;
                    Some(format!("-{}ay{}", word_start, char))
                } else {
                    Some(String::from(char))
                }
            }
        };
        if let Some(more) = maybe_more {
            solution = solution + &more;
        }
        maybe_previous = Some(char);
    }

    solution.truncate(solution.len() - 1);
    solution
}

#[cfg(test)]
mod tests_two {
    use super::solve_two;

    #[test]
    fn one() {
        assert_eq!(
            "e-way used-hay o-tay eet-may every-hay hursday-Tay",
            solve_two("we used to meet every Thursday")
        );
    }

    #[test]
    fn capital_voewl() {
        assert_eq!("Apple-hay", solve_two("Apple"));
    }
}

enum Command {
    Exit,
    ShowAll,
    ShowDepartment(String),
    Add(String, String),
}

fn parse_three(command: &String) -> Option<Command> {
    let command = command.trim();
    let lower = command.to_lowercase();
    if lower == "exit" || lower == "quit" {
        Some(Command::Exit)
    } else if lower == "show" {
        Some(Command::ShowAll)
    } else if lower.starts_with("show ") {
        Some(Command::ShowDepartment(String::from(&lower[5..]).trim().to_string()))
    } else if lower.starts_with("add ") {
        let mut name = String::new();
        let mut rest = String::new();
        for char in lower[4..].chars() {
            if rest.len() > 0 || char.is_ascii_whitespace() {
                rest.push(char);
            } else {
                name.push(char);
            }
        }
        let trimmed = rest.trim();
        if !trimmed.starts_with("to ") {
            return None;
        }
        Some(Command::Add(name, trimmed[3..].to_string()))
    } else {
        None
    }
}

fn print_department(company: &HashMap<String, Vec<String>>, name: &String) {
    if let Some(members) = company.get(name) {
        println!(
            "The '{}' department has {} members:", name, members.len()
        );
        for member in members {
            println!("- {}", member);
        }
    } else {
        println!("The '{}' department has 0 members.", name);
    }
}

// Using a hash map and vectors, create a text interface to allow a user to add
// employee names to a department in a company. For example, “Add Sally to
// Engineering” or “Add Amir to Sales.” Then let the user retrieve a list of
// all people in a department or all people in the company by department,
// sorted alphabetically.
pub fn solve_three() {
    let mut company: HashMap<String, Vec<String>> = HashMap::new();

    loop {
        let mut command = String::new();
        print!("> ");
        io::stdout().flush().unwrap();

        io::stdin()
            .read_line(&mut command)
            .expect("Failed to read line");

        match parse_three(&command) {
            Some(Command::Exit) => break,
            Some(Command::ShowAll) => {
                for department in company.keys() {
                    print_department(&company, &department);
                }
            },
            Some(Command::ShowDepartment(department)) => {
                print_department(&company, &department);
            },
            Some(Command::Add(name, department)) => {
                let mem = company.entry(department).or_insert(Vec::new());
                let mut insertion_index = mem.len();
                for (index, other) in mem.iter().enumerate() {
                    if name < other.to_string() {
                        insertion_index = index;
                        break;
                    }
                }
                mem.insert(insertion_index, name);
            },
            None => println!("Command not recognized")
        }
    }
}
