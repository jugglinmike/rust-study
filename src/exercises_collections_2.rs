use std::str::FromStr;
use std::collections::HashMap;

#[derive(Debug, PartialEq)]
pub enum OfficeDirError {
    ParseError,
    ValueError,
}

impl std::error::Error for OfficeDirError {}

impl std::fmt::Display for OfficeDirError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let name = match self {
            OfficeDirError::ParseError => "Command parsing",
            OfficeDirError::ValueError => "Value",
        };

        write!(f, "{} error", name)
    }
}

#[derive(Debug)]
enum Command {
    Add { person: String, department: String },
    List(Option<String>),
}

impl FromStr for Command {
    type Err = OfficeDirError;

    fn from_str(s: &str) -> Result<Self, OfficeDirError> {
        let trimmed = s.trim();

        if let Some(rest) = trimmed.strip_prefix("add") {
            let (first, second) = rest
                .split_once(" to ")
                .ok_or(OfficeDirError::ParseError)?;

            Ok(Self::Add {
                person: first.trim().to_owned(),
                department: second.trim().to_owned(),
            })
        } else if let Some(rest) = trimmed.strip_prefix("list") {
            if rest.is_empty() {
                Ok(Self::List(None))
            } else {
                Ok(Self::List(Some(rest.trim().to_owned())))
            }
        } else {
            Err(OfficeDirError::ParseError)
        }
    }
}

fn print_department(company: &HashMap<String, Vec<String>>, name: &str) -> Result<String, OfficeDirError> {
    let department = company
        .get(name)
        .ok_or(OfficeDirError::ValueError)?;

    Ok(format!("{}: {}", name, department.join(", ")))
}

pub fn three(command_strs: Vec<&str>) -> Result<String, OfficeDirError> {
    let mut company: HashMap<String, Vec<String>> = HashMap::new();
    let commands = command_strs.iter().map(|c| c.parse::<Command>().unwrap());
    let mut output: Vec<String> = vec![];


    for command in commands {
        match command {
            Command::Add { person, department } => {
                let department = company.entry(department)
                    .or_insert(vec![]);
                let neighbor = department.iter()
                    .enumerate()
                    .find(|(index, name)| name > &&person);
                let index = match neighbor {
                    Some((index, _)) => index,
                    None => department.len(),
                };
                department.insert(index, person);
            },
            Command::List(None) => {
                let mut x = company
                    .iter()
                    .collect::<Vec<(&String, &Vec<String>)>>();

                x.sort_by(|a, b| a.0.partial_cmp(b.0).unwrap());

                for (name, people) in x {
                    output.push(print_department(&company, &name)?);
                }
            },
            Command::List(Some(name)) => {
                output.push(print_department(&company, &name)?);
            },
        };
    }

    output.push(String::from(""));
    Ok(output.join("\n"))
}

fn is_ascii_vowel(c: char) -> bool {
    match c.to_ascii_lowercase() {
        'a' | 'e' | 'i' | 'o' | 'u' | 'y' => true,
        _ => false,
    }
}

fn is_inside_boundary(curr: char, other: Option<char>) -> bool {
    !curr.is_ascii_whitespace() &&
        (other.is_none() || other.unwrap().is_ascii_whitespace())
}

pub fn two(words: &str) -> String {
    let mut pig = String::from("");

    let prev_iter = std::iter::once(None)
        .chain(words.chars().map(|c| Some(c)));
    let curr_iter = words.chars();
    let next_iter = words.chars().map(|c| Some(c))
        .skip(1)
        .chain(std::iter::once(None));
    let iter = prev_iter
        .zip(curr_iter)
        .zip(next_iter)
        .map(|((prev, curr), next)| (prev, curr, next));

    let mut stored: Option<char> = None;

    for (prev, curr, next) in iter {
        let prev_is_empty = prev.is_none() || prev.unwrap().is_ascii_whitespace();

        if is_inside_boundary(curr, prev) {
            if is_ascii_vowel(curr) {
                stored = Some('h');
                pig.push(curr);
            } else {
                stored = Some(curr);
            }
        } else {
            pig.push(curr);
        }

        if is_inside_boundary(curr, next) {
            pig = format!("{}-{}{}", pig, stored.unwrap(), "ay");
        }
    }

    pig
}

#[derive(Debug)]
#[derive(PartialEq)]
pub struct OneAnswer {
    pub median: i32,
    pub mode: i32,
}

pub fn one(numbers: &Vec<i32>) -> Option<OneAnswer> {
    let mut sorted: Vec<i32> = numbers.iter().copied().collect();
    sorted.sort();
    let median = sorted.get(numbers.len() / 2);
    let mut mode = None;
    let mut mode_count = 0;
    let mut counts = HashMap::new();

    for number in &sorted {
        let count = counts.entry(number).or_insert(0);
        *count += 1;

        if count > &mut mode_count {
            mode = Some(*number);
            mode_count = *count;
        }
    }

    match (median, mode) {
        (Some(&median), Some(mode)) => Some(OneAnswer { median, mode }),
        (_, _) => None,
    }
}

fn one_more_advanced_than_i_can_handle_right_now() {
    #[derive(Debug)]
    struct Answer<'t, T> {
        median: &'t T,
        mode: &'t T,
    }
    fn solve<'t, T>(numbers: &'t Vec<T>) -> Answer<'t, T> {
        Answer {
            median: &numbers[0],
            mode: &numbers[0]
        }
    }

    println!("Solution: {:?}", solve(&vec!(1, 2, 3)));
}
