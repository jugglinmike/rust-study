pub fn with_chars() {
    let x = "Mike Pennisi";
    let y = &x[2..5];
    for char in y.chars() {
        println!("So '{}'", char);
    }
    let mut z = y.chars();
    println!("Okay {}", z.next().unwrap_or('X'));
    println!("Okay {}", z.next().unwrap_or('X'));
    println!("Okay {}", z.next().unwrap_or('X'));
    println!("Okay {}", z.next().unwrap_or('X'));
    println!("Okay {}", z.next().unwrap_or('X'));
    println!("Soo {}", y == "ke ");

    println!("And {}", y.chars().skip(1).next() == None);
    println!("And {}", y.chars().skip(2).next() == None);
    println!("And {}", y.chars().skip(3).next() == None);
    println!("And {}", y.chars().skip(4).next() == None);

}

pub fn type_of_chars() {
    {
        let x = "asd".chars().enumerate();
        print_type_of(&x);
    }

}

// https://stackoverflow.com/questions/21747136/how-do-i-print-the-type-of-a-variable-in-rust
fn print_type_of<T>(_: &T) {
    println!("{}", std::any::type_name::<T>())
}

pub fn copying() {
    let mut x = "the world is not enough".chars();
    {
        let y = x.by_ref().take(3);
        println!("y: '{}'", y.collect::<String>());
    }
    println!("x: '{}'", x.collect::<String>());
}
