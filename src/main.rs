use std::io;
mod dual_iter;
mod exercises_collections;
mod exercises_collections_2;
mod memoizer;
mod iterator_stuff;
mod buf_read;
mod read_utf8;
mod windowed_read;
mod slices;
mod sparse_zip;

fn main() {
    //let stdin = io::stdin();
    //buf_read::basic(&mut stdin.lock());

    //dual_iter::both();

    //println!("{:?}", iterator_stuff::first("abc".chars().enumerate()));
    //println!("{:?}", iterator_stuff::first(vec![(3, 'x')].into_iter()));

    //read_utf8::main();

    windowed_read::main();
}

fn collection_challenge2() {
    use exercises_collections_2::{OneAnswer, one, two, three};

    assert_eq!(Some(OneAnswer { median: 2, mode: 1 }), one(&vec!(1, 2, 3)));
    assert_eq!(Some(OneAnswer { median: 2, mode: 1 }), one(&vec!(2, 3, 1)));
    assert_eq!(Some(OneAnswer { median: 3, mode: 1 }), one(&vec!(1, 2, 3, 4)));
    assert_eq!(Some(OneAnswer { median: 3, mode: 1 }), one(&vec!(1, 2, 3, 4, 5)));
    assert_eq!(Some(OneAnswer { median: 2, mode: 2 }), one(&vec!(2, 2, 2, 4, 5)));

    assert_eq!(two("first"), "irst-fay");
    assert_eq!(two("apple"), "apple-hay");
    assert_eq!(two("  first  apple  "), "  irst-fay  apple-hay  ");

    assert_eq!(
        three(vec![
            "add sally to accounting",
            "list accounting",
            "add bob to accounting",
            "list accounting",
            "add xavier to accounting",
            "list accounting",
        ]),
        Ok(String::from("accounting: sally\naccounting: bob, sally\naccounting: bob, sally, xavier\n"))
    );

    assert_eq!(
        three(vec![
            "add sally to engineering",
            "add bob to accounting",
            "add xavier to accounting",
            "list",
        ]),
        Ok(String::from("accounting: bob, xavier\nengineering: sally\n"))
    );
}

fn hash() {
    use std::collections::HashMap;

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    for (key, &(mut value)) in &scores {
        value = 40;
    }

    for (key, value) in &scores {
        println!("{}: {}", key, value);
    }
}

fn refref() {
    fn move_and_print(x: String) {
        println!("{}", x);
    }
    fn borrow_and_print(x: &String) {
        println!("{}", x);
    }

    let value = String::from("hello");
    borrow_and_print(&value);
    move_and_print(value);
}

fn collections() {
    exercises_collections::solve_one(vec!(1));
    exercises_collections::solve_one(vec!(1, 2));
    exercises_collections::solve_one(vec!(1, 2, 3));
    exercises_collections::solve_one(vec!(3, 2, 3, 1, 9, 4, 5, 8, 6));
    exercises_collections::solve_one(vec!(3, 2, 3, 1, 9, 4, 5, 8, 6, 7));
    println!("{}", exercises_collections::solve_two("we used to meet every Thursday"));
    println!("{}", exercises_collections::solve_two("Apple"));
    exercises_collections::solve_three();
}

fn _blah1() {
    fn calculate_length(s: String) -> (String, usize) {
        let length = s.len(); // len() returns the length of a String

        (s, length)
    }
    let s1 = String::from("hello");

    let (s1, len) = calculate_length(s1);

    println!("The length of '{}' is {}.", s1, len);
}

fn _blah2() {
    #[derive(Debug)]
    enum UsState {
        Alabama,
        Alaska,
        // --snip--
    }

    enum Coin {
        Penny,
        Nickel,
        Dime,
        Quarter(UsState),
    }

    fn value_in_cents(coin: Coin) -> u8 {
        match coin {
            Coin::Penny => 1,
            Coin::Nickel => 5,
            Coin::Dime => 10,
            Coin::Quarter(state) => {
                println!("State quarter from {:?}!", state);
                //println!("State quarter from {:?}!", coin.0);
                25
            }
        }
    }

    let mut s = String::from("hello");

    let r1 = &s; // no problem
    let r2 = &s; // no problem
    println!("{} and {}", r1, r2);
    // r1 and r2 are no longer used after this point

    let r3 = &mut s; // no problem
    println!("{}", r3);

    println!("{}", value_in_cents(Coin::Quarter(UsState::Alaska)));
}

fn _guess() {
    println!("Guess the number");
    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    println!("You guessed: {}", guess);
    println!("0.1 + 0.2: {}", 0.1 + 0.2);

}
