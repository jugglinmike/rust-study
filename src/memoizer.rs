use std::marker::PhantomData;
use std::collections::HashMap;

struct Memoizer<T, U, V>
where
    T: Fn(U) -> V,
    U: Copy + std::cmp::Eq + std::hash::Hash,
    V: Copy,
{
    calculation: T,
    values: HashMap<U, V>,
    // https://doc.rust-lang.org/std/marker/struct.PhantomData.html#unused-type-parameters
    p: PhantomData<U>,
}

impl<T, U, V> Memoizer<T, U, V>
where
    T: Fn(U) -> V,
    U: Copy + std::cmp::Eq + std::hash::Hash,
    V: Copy,
{
    fn new(calculation: T) -> Memoizer<T, U, V> {
        Memoizer {
            calculation,
            values: HashMap::new(),
            p: PhantomData,
        }
    }

    fn value(&mut self, arg: U) -> V {
        match self.values.get(&arg) {
            Some(value) => *value,
            None => {
                let value = (self.calculation)(arg);
                self.values.insert(arg, value);
                value
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::Memoizer;

    #[test]
    fn basic() {
        let mut calculate = Memoizer::new(|x| x + 1);
        assert_eq!(calculate.value(1), 2);
        assert_eq!(calculate.value(1), 2);
    }

    #[test]
    fn generalized_closures() {
        let mut calculate = Memoizer::new(|string: &str| string.len());
        assert_eq!(calculate.value("fo0"), 3);
        assert_eq!(calculate.value("fo0"), 3);
    }

    #[test]
    fn multiple_values() {
        let mut calculate = Memoizer::new(|x| x + 1);
        assert_eq!(calculate.value(1), 2);
        assert_eq!(calculate.value(2), 3);
    }
}
