use std::io::{Error, ErrorKind};

const B0000_0000: u8 = 0;
const B1000_0000: u8 = 1 << 7;
const B1100_0000: u8 = 3 << 6;
const B1110_0000: u8 = 7 << 5;
const B1111_0000: u8 = 15 << 4;

pub struct ReadUtf8<T: std::io::Read> {
    buffer: [Option<u8>; 4],
    bytes: std::io::Bytes<T>,
}

impl<T: std::io::Read> ReadUtf8<T> {
    fn code_point(&self) -> Option<Result<u32, Error>> {
        let first = match self.buffer[0] {
            None => return None,
            Some(value) => value,
        };

        if first < B1100_0000 {
            return Some(Ok(first as u32));
        }

        let second = match self.buffer[1] {
            None => return None,
            Some(value) => value,
        };

        if second < 1 << 7 {
            return Some(Err(Error::new(ErrorKind::InvalidInput, "Nope")));
        } else if second >= 3 << 6 {
            return Some(Err(Error::new(ErrorKind::InvalidInput, "Nope")));
        }

        if first < B1110_0000 {
            return Some(Ok(
                (((first - B1100_0000) as u32) << 6) +
                (((second - B1000_0000) as u32))
            ));
        }

        let third = match self.buffer[2] {
            None => return None,
            Some(value) => value,
        };

        if first < B1111_0000 {
            return Some(Ok(
                (((first - B1110_0000) as u32) << 12) +
                (((second - B1000_0000) as u32) << 6) +
                (((third - B1000_0000) as u32))
            ));
        }

        match self.buffer[3] {
            None => None,
            Some(fourth) => Some(Ok(
                (((first - B1111_0000) as u32) << 18) +
                (((second - B1000_0000) as u32) << 12) +
                (((third - B1000_0000) as u32) << 6) +
                ((fourth - B1000_0000) as u32)
            )),
        }
    }
}

impl<T: std::io::Read> ReadUtf8<T> {
    pub fn new(source: T) -> Self {
        Self {
            buffer: [None; 4],
            bytes: source.bytes(),
        }
    }
}

impl<T: std::io::Read> Iterator for ReadUtf8<T> {
    type Item = Result<char, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let byte = match self.bytes.next() {
            Some(Ok(byte)) => byte,
            Some(Err(error)) => return Some(Err(error)),
            None => return None,
        };

        match self.buffer {
            [None, _, _, _] => { self.buffer[0] = Some(byte); },
            [_, None, _, _] => { self.buffer[1] = Some(byte); },
            [_, _, None, _] => { self.buffer[2] = Some(byte); },
            [_, _, _, None] => { self.buffer[3] = Some(byte); },
            _ => return Some(Err(Error::new(ErrorKind::InvalidInput, "what"))),
        };

        match self.code_point() {
            Some(Err(error)) => Some(Err(error)),
            Some(Ok(code_point)) => {
                self.buffer[0] = None;
                self.buffer[1] = None;
                self.buffer[2] = None;
                self.buffer[3] = None;

                match char::from_u32(code_point) {
                    None => None,
                    Some(c) => Some(Ok(c)),
                }
            }
            None => self.next(),
        }
    }
}

pub fn main() {
    let tests: Vec<&[u8]> = vec![
        &[50, 70, 98], // "2Fb"
        &[36], // "$"
        &[194, 163], // "£"
        &[240, 159, 146, 150], // sparkle heart
        &[97, 240, 159, 146, 150], // "a", then sparkle heart
    ];

    for test in tests {
        let x = ReadUtf8::new(test);

        for item in x {
            println!("{:?}", item);
        }
    }
}


#[cfg(test)]
mod read_utf8 {
    // TODO: Test errors
    use super::{Error, ErrorKind, ReadUtf8};

    fn run(input: &[u8]) -> Vec<char> {
        ReadUtf8::new(input)
            .into_iter()
            .map(|elem| elem.unwrap())
            .collect()

    }

    #[test]
    fn one_byte() {
        assert_eq!(
            run(&[50, 70, 98]),
            vec!['2', 'F', 'b'],
        );
    }

    #[test]
    fn two_bytes() {
        assert_eq!(
            run(&[194, 163]),
            vec!['£'],
        );
    }

    #[test]
    fn three_bytes() {
        assert_eq!(
            run(&[226, 130, 172 ]),
            vec!['€'],
        );
    }

    #[test]
    fn four_bytes() {
        assert_eq!(
            run(&[240, 159, 146, 150]),
            vec!['💖'],
        );
    }
}
