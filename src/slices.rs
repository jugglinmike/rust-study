pub fn things<'a>(first: &'a [i32], second: &'a [i32]) -> (&'a [i32], &'a [i32]) {
    return (&first[1..], &second[..2]);
}
