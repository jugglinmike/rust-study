pub struct SparseZip<'a, T, U> {
    a: &'a mut Iterator<Item=T>,
    b: &'a mut Iterator<Item=U>,
}

impl<'a, T, U> SparseZip<'a, T, U> {
    pub fn new(a: &'a mut Iterator<Item=T>, b: &'a mut Iterator<Item=U>) -> SparseZip<'a, T, U> {
        SparseZip {
            a: a,
            b: b,
        }
    }
}

impl<'a, T, U> Iterator for SparseZip<'a, T, U> {
    type Item = (Option<T>, Option<U>);

    fn next(&mut self) -> Option<Self::Item> {
        match (self.a.next(), self.b.next()) {
            (None, None) => None,
            (a, b) => Some((a, b)),
        }
    }
}

#[cfg(test)]
mod sparse_zip_tests {
    use super::SparseZip;

    #[test]
    fn equal_length() {
        let mut first = "foo".chars();
        let mut second = "bar".chars();
        let zipped = SparseZip::new(&mut first, &mut second);

        assert_eq!(
            zipped.collect::<Vec<_>>(),
            vec![(Some('f'), Some('b')), (Some('o'), Some('a')), (Some('o'), Some('r'))]
        );
    }

    #[test]
    fn both_empty() {
        let mut first = "".chars();
        let mut second = "".chars();
        let zipped = SparseZip::new(&mut first, &mut second);

        assert_eq!(zipped.collect::<Vec<_>>(), vec![]);
    }

    #[test]
    fn first_short() {
        let mut first = "f".chars();
        let mut second = "bar".chars();
        let zipped = SparseZip::new(&mut first, &mut second);

        assert_eq!(
            zipped.collect::<Vec<_>>(),
            vec![(Some('f'), Some('b')), (None, Some('a')), (None, Some('r'))]
        );
    }

    #[test]
    fn second_short() {
        let mut first = "foo".chars();
        let mut second = "b".chars();
        let zipped = SparseZip::new(&mut first, &mut second);

        assert_eq!(
            zipped.collect::<Vec<_>>(),
            vec![(Some('f'), Some('b')), (Some('o'), None), (Some('o'), None)]
        );
    }

    #[test]
    fn first_empty() {
        let mut first = "".chars();
        let mut second = "bar".chars();
        let zipped = SparseZip::new(&mut first, &mut second);

        assert_eq!(
            zipped.collect::<Vec<_>>(),
            vec![(None, Some('b')), (None, Some('a')), (None, Some('r'))]
        );
    }

    #[test]
    fn second_empty() {
        let mut first = "foo".chars();
        let mut second = "".chars();
        let zipped = SparseZip::new(&mut first, &mut second);

        assert_eq!(
            zipped.collect::<Vec<_>>(),
            vec![(Some('f'), None), (Some('o'), None), (Some('o'), None)]
        );
    }
}
