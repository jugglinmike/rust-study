// This is poorly-conceived, partially implemented, and unlikely to ever be
// useful.
const LINE_FEED: u8 = 0x0A;

struct Windowed<T: std::io::Read> {
    pub window: Vec<u8>,
    read: T,
}

impl<T: std::io::Read> Windowed<T> {
    pub fn new(read: T) -> Windowed<T> {
        Windowed {
            window: vec![],
            read
        }
    }
}

impl<T: std::io::Read> std::io::Read for Windowed<T> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, std::io::Error> {
        let count = self.read.read(buf)?;
        for index in 0..count {
            if buf[index] == LINE_FEED {
                self.window.clear();
            } else {
                self.window.push(buf[index]);
            }
        }

        Ok(count)
    }
}

pub fn main() {
    let x = Windowed::new("hello".as_bytes());

    for result in std::io::Read::bytes(x) {
        println!("result: {:?}", result);
    }
}

#[cfg(test)]
mod windowed_test {
    use super::Windowed;

    fn run(input: &str, offset: usize) -> (char, String) {
        let mut x = Windowed::new(input.as_bytes());
        let mut buffer = vec![0; offset + 1];

        std::io::Read::read_exact(&mut x, &mut buffer);

        (
            buffer[offset] as char,
            std::str::from_utf8(&x.window).unwrap().to_string()
        )
    }

    #[test]
    fn first_char_first_line() {
        assert_eq!(
            run("a", 0),
            ('a', "a".to_string()),
        );
    }

    #[test]
    fn second_char_first_line() {
        assert_eq!(
            run("ab", 1),
            ('b', "ab".to_string()),
        );
    }

    #[test]
    fn first_newline() {
        assert_eq!(
            run("ab\n", 2),
            ('\n', "".to_string()),
        );
    }

    #[test]
    fn first_char_second_line() {
        assert_eq!(
            run("ab\nc", 3),
            ('c', "c".to_string()),
        );
    }

    #[test]
    fn second_char_second_line() {
        assert_eq!(
            run("ab\ncd", 4),
            ('d', "cd".to_string()),
        );
    }
}
